terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }
  backend "http" {} 
}
provider "aws" {
  region = "us-east-1"
}



variable "group_ids" {
  type    = map(object({
    instance_profile_role_name = string
    groups                      = list(string)
  }))
  default = {}
}

locals {
  role_names = length(var.group_ids) > 0 ? { for key, value in var.group_ids : value.instance_profile_role_name => value.groups } : {}
  
  group_names = length(var.group_ids) > 0 ? merge([for key, value in var.group_ids : { for group in value.groups : group = value.instance_profile_role_name }]) : {}
}